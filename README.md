C9 Install - How to create the docker image
================================================
```
apt-get install -y build-essential g++ curl libssl-dev apache2-utils git libxml2-dev
apt-get install python
git clone git://github.com/creationix/nvm.git ~/nvm
git clone git://github.com/c9/core.git c9sdk
cd c9sdk
scripts/install-sdk.sh
```

Download and unpack the docker file
===================================
```
git clone git@bitbucket.org:nerddesire/c9-docker.git
cd c9-docker
gzip -d cloud9-docker.tar.gz
```

How to Run the docker
====================
```
docker load --input cloud9-docker.tar
docker tag <IMAGE ID> ubuntu:cloud9
docker run -i -p 80:8080 ubuntu:cloud9 /bin/bash -c "/root/nvm/versions/node/v0.12.12/bin/node /root/c9sdk/server.js --port 8080 --listen 0.0.0.0 --auth test:test --debug -w /home/git"
```

Example
=========
```
Running apache
docker run -d -p 8080:80 kravivar/apache /usr/sbin/apache2ctl -D FOREGROUND
```

Reference
=========
http://slopjong.de/2014/09/17/install-and-run-a-web-server-in-a-docker-container/

https://github.com/creationix/nvm

https://www.samclarke.com/2012/07/how-to-install-cloud-9-ide-on-ubuntu-12-04-lts-precise-pangolin/

https://github.com/c9/core

https://github.com/docker/docker/issues/13625